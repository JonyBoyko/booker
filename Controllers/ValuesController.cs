﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using jBooker.Models;
using Newtonsoft.Json.Linq;

namespace jBooker.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {

        BookContext _db = new BookContext();

        // GET api/values
        [Route("books")]
        public IEnumerable<BookModel> GetBooks()
        {
            return _db.Books;
        }
        [Route("books/{id}")]
        public BookModel GetBook(int id)
        {
            BookModel book = _db.Books.Find(id);
            return book;
        }

        [Route("books")]
        [HttpPost]
        public IHttpActionResult CreateBook([FromBody]BookModel book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("bad request");
            }

            _db.Books.Add(book);
            _db.SaveChanges();
            return Ok(book);
        }

        [Route("books/{id}")]
        [HttpPut]
        public IHttpActionResult EditBook(int id, [FromBody] BookModel book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("bad request");
            }

            var toUpdateBook = _db.Books.Find(id);

            if (toUpdateBook == null)
            {
                return NotFound();
            }

            toUpdateBook.Author = book.Author;
            toUpdateBook.Name = book.Name;
            toUpdateBook.Year = book.Year;
            _db.SaveChanges();
            return Ok(toUpdateBook);
        }

        [Route("books/{id}")]
        public void DeleteBook(int id)
        {
            BookModel book = _db.Books.Find(id);
            if (book != null)
            {
                _db.Books.Remove(book);
                _db.SaveChanges();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
