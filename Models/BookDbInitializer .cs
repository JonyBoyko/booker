﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace jBooker.Models
{
    public class BookDbInitializer : DropCreateDatabaseAlways<BookContext>
    {

        protected override void Seed(BookContext db)
        {
            db.Books.Add(new BookModel { Name = "Война и мир", Author = "Л. Толстой", Year = 1863 });
            db.Books.Add(new BookModel { Name = "Отцы и дети", Author = "И. Тургенев", Year = 1862 });
            db.Books.Add(new BookModel { Name = "Чайка", Author = "А. Чехов", Year = 1896 });

            base.Seed(db);
        }
    }
}