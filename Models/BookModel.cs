﻿using System.ComponentModel.DataAnnotations;

namespace jBooker.Models
{
    public class BookModel
    {        
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        [Range(1400,2017)]
        public short Year { get; set; }

    }
}