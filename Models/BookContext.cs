﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace jBooker.Models
{
    public class BookContext : DbContext
    {

        public BookContext(): base("BookContext") 
    {
            
            Database.SetInitializer<BookContext>(new BookDbInitializer());
         
        }
        public DbSet<BookModel> Books { get; set; }
    }
}