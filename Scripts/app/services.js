function BookService($q, $http) {
    var BOOK_RES_URL = 'books/';

    var books = [];
    var originalBooks = [];

    return {
        loadBooks: loadBooks,
        deleteBook: deleteBook,
        getAllBooks: getAllBooks,
        getBook: getBook,
        saveOUpdateBook: saveOUpdateBook,
        filterBooksBy: filterBooksBy,
        clearFilters: clearFilters
    };

    function getBook(bookId) {
        return originalBooks[findBookIndexBy(bookId)];
    }

    function filterBooksBy(searchObj) {
        books = originalBooks.filter(function (book) {
            return Object.keys(searchObj).reduce(function(prev, key) {
                var bookKey = book[key];
                var searchToken = searchObj[key];
                return prev || (bookKey.indexOf(searchToken) !== -1);
            }, false);
        });
    }

    function clearFilters() {
        books = angular.copy(originalBooks);
    }

    function loadBooks() {
        var loadTask = $q.defer();
        $http.get(BOOK_RES_URL).success(function (data) {
            books = data;
            originalBooks = data;
            loadTask.resolve(books);
        }).error(function(err){
            console.log(err);
        });
        
        return loadTask.promise;
    }
    
    function saveOUpdateBook(book) {
        var isNew = book.Id === 0;
        return $http[isNew? 'post' : 'put'](BOOK_RES_URL + (isNew? '' : book.Id), book).then(function (response) {
            updateBookCollection(response.data);
        });
    }

    function updateBookCollection(book) {
        var index = findBookIndexBy(book.Id);
        if (index !== -1) {
            originalBooks.splice(index, 1, book);
        } else {
            originalBooks.push(book);
        }
    }

    function findBookIndexBy(bookId) {
        return originalBooks.findIndex(function (toDeleteBook) {
            return toDeleteBook.Id === bookId;
        });
    }
    
    function deleteBook(bookId) {
        var deleteTask = $q.defer();

        var index = findBookIndexBy(bookId);

        if (index !== -1) {
            $http.delete(BOOK_RES_URL + bookId).success(function () {
                originalBooks.splice(index, 1);
                deleteTask.resolve();
            });
        }

        return deleteTask.promise;
    }

    function getAllBooks() {
        return books;
    }
}