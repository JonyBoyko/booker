function BookCtrl($scope, BookService){
    var vm = this;

    angular.extend(vm, {
        addNewBook: addNewBook,
        deleteBook: deleteBook,
        editBook: editBook,
        saveBook: saveBook,
        getBooks: getBooks,
        init: init
    });
    
    function init() {
        BookService.loadBooks().then(function () {
            $scope.loaded = true;

            $scope.$on('$destroy', $scope.$watch('search', filter));
        });
    }

    function filter() {
        if ($scope.search) {
            BookService.filterBooksBy({ Author: $scope.search, Name: $scope.search });
        } else {
            BookService.clearFilters();
        }
    }
    
    function getBooks() {
        return BookService.getAllBooks();
    }
    
    function addNewBook() {
        $scope.currentBook = { Id: 0 };
    }

    function cleanUpForm() {
        $scope.currentBook = {
            id: -1
        };
    }

    function saveBook(currentBook) {
        BookService.saveOUpdateBook(currentBook).then(cleanUpForm).then(filter);
    }

    function deleteBook(bookId){
        BookService.deleteBook(bookId).then(filter);
    }

    function editBook(bookId){
        $scope.currentBook = angular.extend({}, BookService.getBook(bookId));
    }
}